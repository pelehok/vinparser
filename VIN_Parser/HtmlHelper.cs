﻿using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIN_Parser
{
    class HtmlHelper
    {
        private static string _html { get; set; }
        private static string _url { get; set; }
        public static string GetHtml(string url)
        {
            _url = url;

            Thread thread = new Thread(new ThreadStart(GetHtmlWorker));

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
            return _html;
        }

        protected async static void GetHtmlWorker()
        {
            using (WebBrowser browser = new WebBrowser())
            {
                browser.ScriptErrorsSuppressed = true;
                browser.Navigate(_url);

                while (browser.ReadyState != WebBrowserReadyState.Complete)
                {
                    Application.DoEvents();
                }

                var documentElement = browser.Document.GetElementsByTagName("html")[0];
                _html = documentElement.OuterHtml;

                while (true)
                {
                    await Task.Delay(3000);
                    if (browser.IsBusy)
                        continue;

                    var htmlNow = documentElement.OuterHtml;

                    if (_html == htmlNow)
                        break; 
                    _html = htmlNow;
                }
            }
        }
    }
}
