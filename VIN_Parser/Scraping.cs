﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace VIN_Parser
{
    class Scraping
    {
        private string _baseUrlFirstSite = "https://vindecoder.eu/check-vin";
        private string _baseUrlSecondSite = "https://vpic.nhtsa.dot.gov/api/vehicles/decodevinextended";
        private const string _patternForMaker = "<div.*data-vd-vin-detail=\"Make\">(.*)<\\/div>";
        private const string _patternForYear = "<div.*data-vd-vin-detail=\"Model Year\">(.*)<\\/div>";
        private const string _patternForModel = "Model:.*<\\/span>\n*.*<\\/div>\n.*\n*<div.*>(.*)<\\/div>";
        private const string _dontFindMessage = "Don't find";

        private string GetValue(string pattern, string text)
        {
            Regex regex = new Regex(pattern);

            Match match = regex.Match(text);

            if (match.Success)
            {
                string key = match.Groups[1].Value;
                return key;
            }
            return "Don't find";
        }
        public List<VIN> WorkerWithRegex(List<string> vinNumbers)
        {
            List<VIN> result = new List<VIN>();

            for (int i = 0; i < vinNumbers.Count; i++)
            {
                try
                {
                    string url = $"{_baseUrlFirstSite}/{vinNumbers[i]}/";
                    string html = HtmlHelper.GetHtml(url);
                    //string html = System.IO.File.ReadAllText(@"I:\textfromfile.txt");

                    string maker = GetValue(_patternForMaker, html);

                    string year = GetValue(_patternForYear, html);

                    string model = GetValue(_patternForModel, html);

                    if (model == null && maker == null && year == null)
                    {
                        Console.WriteLine("Page don't work");
                        return result;
                    }

                    result.Add(new VIN() { Number = vinNumbers[i], Maker = maker, Model = model, Year = year });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    break;
                }
            }
            return result;
        }
        public List<VIN> WorkerWithXml(List<string> vinNumbers)
        {
            List<VIN> result = new List<VIN>();

            for (int i = 0; i < vinNumbers.Count; i++)
            {
                string model = "";
                string maker = "";
                string year = "";
                try
                {
                    string url = $"{_baseUrlSecondSite}/{vinNumbers[i]}?format=xml";
                    string xml = XmlHelper.GetStringXml(url);

                    XElement root = XElement.Parse(xml);

                    XElement resultElement = (XElement)root.LastNode;

                    IEnumerable<XElement> decodedElements = resultElement.Elements("DecodedVariable");

                    foreach (XElement decodedElement in decodedElements)
                    {
                        XElement valueModel = getXElement(decodedElement, "Model");
                        XElement valueMake = getXElement(decodedElement, "Make");
                        XElement valueYear = getXElement(decodedElement, "Model Year");
                        if (valueModel != null)
                        {
                            model = getXElementValue(decodedElement);
                        }else if (valueMake != null)
                        {
                            maker = getXElementValue(decodedElement);
                        }
                        else if(valueYear!=null)
                        {
                            year = getXElementValue(decodedElement);
                        }
                    }

                    result.Add(new VIN() {
                        Number = (vinNumbers[i]!=null? vinNumbers[i] : _dontFindMessage),
                        Maker = (maker != "" ? maker : _dontFindMessage),
                        Model = (model != "" ? model : _dontFindMessage),
                        Year = (year != "" ? year : _dontFindMessage)
                    });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    break;
                }
            }
            return result;
        }
        private XElement getXElement(XElement elementParent, string Value)
        {
            XElement resultElement = elementParent.Elements().Where(x => x.Name == "Variable" && x.Value == Value).FirstOrDefault();
            if (resultElement != null)
            {
                return elementParent.Elements().Where(x => x.Name == "Variable").First();
            }
            else
            {
                return null;
            }
        }
        private string getXElementValue(XElement elementParent)
        {
            XElement obj = elementParent.Elements().Where(x => x.Name == "Value").FirstOrDefault();
            if (obj != null)
            {
                return obj.Value.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}
