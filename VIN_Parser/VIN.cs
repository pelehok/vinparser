﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VIN_Parser
{
    class VIN
    {
        public string Number { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string ToString()
        {
            return "Number = " + Number + 
                "\r\nMAker = " + Maker + 
                "\r\nModel = " + Model + 
                "\r\nYear = " +Year;
        }
    }
}
